// Coding Challenge 130.3: Drawing with Fourier Transform and Epicycles
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/130.1-fourier-transform-drawing.html
// https://thecodingtrain.com/CodingChallenges/130.2-fourier-transform-drawing.html
// https://thecodingtrain.com/CodingChallenges/130.3-fourier-transform-drawing.html
// https://youtu.be/7_vKzcgpfvU
// https://editor.p5js.org/codingtrain/sketches/ldBlISrsQ
const skip = 3;
let fourier;
let time;
let complete_time;
let path;
let back_path;
let letters;

function setup() {
 
  width = 800;
  height = 600;
  createCanvas(width, height);
  letters = [
  new Letter(width / 9, height / 4, K , 35,35),
  new Letter(2.2*width / 9, height / 4, O),
  new Letter(3.2*width / 9, height / 4, C),
  new Letter(4.5*width / 9, height / 4, H, 30, 30),
  new Letter(5.9*width / 9, height / 3.9, A, 30, 30),
  new Letter(7.2*width / 9, height / 4, M),
  new Letter(3*width / 9, height / 2, C),
  new Letter(4.2*width / 9, height / 2, I),
  new Letter(5.5*width / 9, height / 2, El),
  new Letter(2.2*width / 9, height / 1.2, I),
  new Letter(3.2*width / 9, height / 1.2, N),
  new Letter(4.7*width / 9, height / 1.2, E),
  new Letter(5.8*width / 9, height / 1.2, Z, 35,35)
    
  ];
  
  path = init_list(letters.length, []);
  back_path = init_list(letters.length, []);
  time = init_time(letters.length);
  complete_time = init_time(letters.length);
  const dt = [];
  for(let s = 0; s < letters.length; s++){ 
    dt.push(TWO_PI / letters[s].fourier.length);
  }
  while(time.some((num) => num < TWO_PI)){
    for(let s = 0; s < letters.length; s++){
    let v = epicycles_just_path(letters[s].x, letters[s].y,  0, letters[s].fourier, time[s]);
    back_path[s].unshift(v);
  }
  
    for(let s = 0; s < time.length; s++){
      time[s] += dt[s]; }
}
 for(let s = 0; s < letters.length; s++){ 
 time[s] = 0;
  }
}




function init_list(len)
{
  let t = [];
  for(let s=0; s<len; s++){t.push([]);}
  return  t;
}

function init_time(len)
{
  let t = [];
  for(let s=0; s<len; s++){t.push(0);}
  return  t;
}


function draw() {
  background(4, 30, 45);
  const dt = [];

  for(let s = 0; s < letters.length; s++){
    draw_path(letters[s], back_path[s], 'rgb(190, 194, 203)');
   // draw_letter_no_epicycles(letters[s], back_path[s], 'rgb(190, 194, 203)', complete_time[s]);
    draw_letter(letters[s], path[s], 'rgb(212, 175, 55)', time[s]);
    dt.push(TWO_PI / letters[s].fourier.length);
  }
 // console.log(complete_time);
  for(let s = 0; s < time.length; s++){
      time[s] += dt[s];
      
      if (time[s] > TWO_PI) {
        time[s] = 0;
        path[s] = [];
      }
   }
}
