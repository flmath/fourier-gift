class Letter {
  constructor(x, y, points, smoothX, smoothY) {
    this.x = x;
    this.y = y;
    let t = [];
    if (smoothX===undefined){smoothX = Infinity;}
    if (smoothY===undefined){smoothY = Infinity;}
    this.smoothX = smoothX;
    this.smoothY = smoothY;
    for (let i = 0; i < points.length; i += skip) {
    const c = new Complex(points[i].x, points[i].y);
    t.push(c);
  }
  this.fourier = dft(t);
  this.fourier.sort((a, b) => b.amp - a.amp);
  }
}

function draw_letter(letter, path, path_color, time){  
  let startx = letter.x;
  let starty = letter.y;
  let fourier = letter.fourier;
  let smoothX = letter.smoothX;
  let smoothY = letter.smoothY;
  let v = epicycles(startx, starty, 0, fourier, time, smoothX, smoothY);
  path.unshift(v);
 
  beginShape();
  noFill();
  stroke(path_color);
  for (let i = 1; i < path.length; i++)
  {
    if(abs(path[i-1].y-path[i].y)>smoothY){noStroke();
    //console.log(abs(path[i-1].y-path[i].y));
    }
    if(abs(path[i-1].x-path[i].x)>smoothX){noStroke();
    //console.log(abs(path[i+1].y-path[i].y));
    }
      line(path[i-1].x, path[i-1].y, path[i].x, path[i].y);
    }
    stroke(path_color);
  endShape();
}

function draw_letter_no_epicycles(letter, path, path_color, time){
  let startx = letter.x;
  let starty = letter.y;
  let fourier = letter.fourier;
  let smoothX = letter.smoothX;
  let smoothY = letter.smoothY;
  let v = epicycles_just_path(startx, starty,  0, fourier, time);
  path.unshift(v);
  beginShape();
  noFill();
  stroke(path_color);
 for (let i = 1; i < path.length; i++)
    { 
 
    if(abs(path[i-1].y-path[i].y)>smoothY){noStroke();
    //console.log(abs(path[i-1].y-path[i].y));
    }
    if(abs(path[i-1].x-path[i].x)>smoothX){noStroke();
    //console.log(abs(path[i+1].y-path[i].y));
    }
    line(path[i-1].x, path[i-1].y, path[i].x, path[i].y);
    stroke(path_color);
    }
  endShape();
}

function draw_path(letter, path, path_color){
  let smoothX = letter.smoothX;
  let smoothY = letter.smoothY;
  beginShape();
  noFill();
  stroke(path_color);
 for (let i = 1; i < path.length; i++)
    { 
 
    if(abs(path[i-1].y-path[i].y)>smoothY){noStroke();
    //console.log(abs(path[i-1].y-path[i].y));
    }
    if(abs(path[i-1].x-path[i].x)>smoothX){noStroke();
    //console.log(abs(path[i+1].y-path[i].y));
    }
    line(path[i-1].x, path[i-1].y, path[i].x, path[i].y);
    stroke(path_color);
    }
  endShape();
}

function epicycles(x, y, rotation, fourier, time, thecolor) {
  for (let i = 1; i < fourier.length; i++) {
    let prevx = x;
    let prevy = y;
    let freq = fourier[i].freq;
    let radius = fourier[i].amp;
    let phase = fourier[i].phase;
    x += radius * cos(freq * time + phase + rotation);
    y += radius * sin(freq * time + phase + rotation);

    stroke(150, 100);
    noFill();
    ellipse(prevx, prevy, radius * 2);
    stroke(150);
    line(prevx, prevy, x, y);
  }
  return createVector(x, y);
}
function epicycles_just_path(x, y, rotation, fourier, time) {
  for (let i = 1; i < fourier.length; i++) {
    let prevx = x;
    let prevy = y;
    let freq = fourier[i].freq;
    let radius = fourier[i].amp;
    let phase = fourier[i].phase;
    x += radius * cos(freq * time + phase + rotation);
    y += radius * sin(freq * time + phase + rotation);
  }
  
  return createVector(x, y);
}
