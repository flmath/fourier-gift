import React from "react"
import { css } from "@emotion/react"

export default function Layout({ children }) {
  return (
    <div
      css={css`
        background-color: rgb(4, 30, 45);
      `}
    >
    
      {children}
    </div>
  )
}