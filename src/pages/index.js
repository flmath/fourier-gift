import React from "react"
import { withPrefix } from "gatsby"
import Layout from "../components/layout"
import Helmet from "react-helmet"
import SEO from "../components/seo"

export default function Home({ data }) {
  return (
    <Layout>
 <Helmet>
        <script src={withPrefix('libraries/p5.min.js')} type="text/javascript" />
        <script src={withPrefix('A.js')} type="text/javascript" />
        <script src={withPrefix('C.js')} type="text/javascript" />
        <script src={withPrefix('E.js')} type="text/javascript" />
        <script src={withPrefix('El.js')} type="text/javascript" />
        <script src={withPrefix('fourier.js')} type="text/javascript" />
        <script src={withPrefix('H.js')} type="text/javascript" />
        <script src={withPrefix('I.js')} type="text/javascript" />
        <script src={withPrefix('K.js')} type="text/javascript" />
        <script src={withPrefix('Letter.js')} type="text/javascript" />
        <script src={withPrefix('M.js')} type="text/javascript" />
        <script src={withPrefix('N.js')} type="text/javascript" />
        <script src={withPrefix('O.js')} type="text/javascript" />
        <script src={withPrefix('sketch_js.js')} type="text/javascript" />
        <script src={withPrefix('Z.js')} type="text/javascript" />
    </Helmet>
    <SEO title="Home" keywords={[`Inez`, `gift`, `fourier`]} />
    
    </Layout>
)
  
}